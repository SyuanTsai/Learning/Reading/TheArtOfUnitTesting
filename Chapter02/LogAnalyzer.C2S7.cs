﻿namespace Chapter02;

public class LogAnalyzerC2S7
{
    public bool WasLastFileNameValid { get; set; }
    public bool IsValidLogFileName(string fileName)
    {
        //  改變系統狀態
        WasLastFileNameValid = false;
        if (string.IsNullOrEmpty(fileName))
        {
            throw new ArgumentException("檔案名稱必須要提供");
        }
        
        if (!fileName.EndsWith(".SLF", StringComparison.CurrentCultureIgnoreCase))
        {
            return false;
        }

        //  改變系統狀態
        WasLastFileNameValid = true;
        return true;
    }
}