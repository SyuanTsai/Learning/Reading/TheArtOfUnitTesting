﻿namespace Chapter02;

public class LogAnalyzer
{
    public bool IsValidLogFileName(string fileName)
    {
        //  2.6.2 驗證預期的例外
        if (string.IsNullOrEmpty(fileName))
        {
            throw new ArgumentException("檔案名稱必須要提供");
        }
        
        // 2.3.2
        // Bug 應該要有!，但是沒有
        // if (fileName.EndsWith(".SLF"))
            
        //  2.4 新增測試並修正Bug 
        //  if (!fileName.EndsWith(".SLF"))
        
        // 2.4.3 增加正向的測試 => 大小寫
        if (!fileName.EndsWith(".SLF", StringComparison.CurrentCultureIgnoreCase))
        {
            return false;
        }
        
        return true;
    }
}