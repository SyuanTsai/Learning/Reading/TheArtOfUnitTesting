﻿using System.Diagnostics;

namespace Chapter03;

public class LogAnalyzerC3S6P2
{
    private IFileExtensionManagerCH3S4P1 _mgr;
    
    public LogAnalyzerC3S6P2(IFileExtensionManagerCH3S4P1 mgr)
    {
        _mgr = mgr;
    }
    
    [Conditional("DEBUG")]
    public void ReplaceFileExtension(IFileExtensionManagerCH3S4P1 mgr)
    {
        _mgr = mgr;
    }
    
    public bool IsValidLogFileName(string fileName)
    {
        return _mgr.IsValid(fileName);
    }
}