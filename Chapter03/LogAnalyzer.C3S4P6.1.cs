﻿namespace Chapter03;

public class LogAnalyzerC3S4P61
{
    private IFileExtensionManagerCH3S4P1 _mgr;

    public LogAnalyzerC3S4P61(ExtensionManagerFactoryC3S4P6 factory)
    {
        _mgr = factory.Create();
    }
    
    public bool IsValidLogFileName(string fileName)
    {
        return _mgr.IsValid(fileName)
            && Path.GetFileNameWithoutExtension(fileName).Length > 5;
    }
}

public class ExtensionManagerFactoryC3S4P6
{
    private IFileExtensionManagerCH3S4P1 customManager = null;
    public IFileExtensionManagerCH3S4P1 Create()
    {
        if (customManager != null)
        {
            return customManager;
        }
        return new FileExtensionManagerCH3S4P1();
    }
    
    //  接縫
    public void SetManager(IFileExtensionManagerCH3S4P1 mgr)
    {
        customManager = mgr;
    }
}