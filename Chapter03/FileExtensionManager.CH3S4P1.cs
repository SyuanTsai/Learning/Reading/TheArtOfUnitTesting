﻿namespace Chapter03;

public interface IFileExtensionManagerCH3S4P1
{
    bool IsValid(string fileName);
}

// CH 3.4.1 抽取介面
public class FileExtensionManagerCH3S4P1 : IFileExtensionManagerCH3S4P1
{
    public bool IsValid(string fileName)
    {
        // read some file
        return false;
    }
}