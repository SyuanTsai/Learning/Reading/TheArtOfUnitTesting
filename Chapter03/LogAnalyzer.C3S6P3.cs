﻿namespace Chapter03;

public class LogAnalyzerC3S6P3
{
    private IFileExtensionManagerCH3S4P1 _mgr;

#if DEBUG
    public LogAnalyzerC3S6P3(IFileExtensionManagerCH3S4P1 mgr)
    {
        _mgr = mgr;
    }
#endif

    public bool IsValidLogFileName(string fileName)
    {
        return _mgr.IsValid(fileName);
    }
}