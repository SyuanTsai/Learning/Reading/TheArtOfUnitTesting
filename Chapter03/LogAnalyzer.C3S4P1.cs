﻿namespace Chapter03;

public class LogAnalyzerC3S4P1
{
    public bool IsValidLogFileName(string fileName)
    {
        IFileExtensionManagerCH3S4P1 mgr = new FileExtensionManagerCH3S4P1();
        return mgr.IsValid(fileName);
    }
}