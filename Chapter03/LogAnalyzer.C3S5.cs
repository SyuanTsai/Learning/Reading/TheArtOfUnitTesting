﻿namespace Chapter03;

public class LogAnalyzerC3S5
{
    private IFileExtensionManagerCH3S4P1 _mgr;

    public LogAnalyzerC3S5(ExtensionManagerFactoryC3S4P6 factory)
    {
        _mgr = factory.Create();
    }

    public bool IsValidLogFileName(string fileName)
    {
        return _mgr.IsValid(fileName)
               && Path.GetFileNameWithoutExtension(fileName).Length > 5;
    }
}

public class LogAnalyzerUsingFactoryMethodC3S5
{
    public bool IsValidLogFileName(string fileName)
    {
        return this.IsValid(fileName);
    }

    protected virtual bool IsValid(string fileName)
    {
        FileExtensionManagerCH3S4 mgr = new FileExtensionManagerCH3S4();
        return mgr.IsValid(fileName);
    }
}