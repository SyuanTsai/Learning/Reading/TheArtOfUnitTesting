﻿namespace Chapter03;

public class LogAnalyzerC3S4P62
{
    private IFileExtensionManagerCH3S4P1 _mgr;

    public LogAnalyzerC3S4P62(ExtensionManagerFactoryC3S4P6 factory)
    {
        _mgr = factory.Create();
    }

    public bool IsValidLogFileName(string fileName)
    {
        return _mgr.IsValid(fileName)
               && Path.GetFileNameWithoutExtension(fileName).Length > 5;
    }
}

public class LogAnalyzerUsingFactoryMethodC3S4P62
{
    public bool IsValidLogFileName(string fileName)
    {
        return GetManager().IsValid(fileName);
    }

    protected virtual IFileExtensionManagerCH3S4P1 GetManager()
    {
        throw new NotImplementedException();
    }
}