﻿namespace Chapter03;

public class LogAnalyzerC3S4
{
    public bool IsValidLogFileName(string fileName)
    {
        FileExtensionManagerCH3S4 mgr = new FileExtensionManagerCH3S4();
        return mgr.IsValid(fileName);
    }
}