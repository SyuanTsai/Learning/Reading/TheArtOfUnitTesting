﻿namespace Chapter03;

public class LogAnalyzerC3S4P3
{
    private IFileExtensionManagerCH3S4P1 _mgr;
    
    // CH 3.4.3 -- 建構函式注入
    public LogAnalyzerC3S4P3(IFileExtensionManagerCH3S4P1 mgr)
    {
        _mgr = mgr;
    }

    public bool IsValidLogFileName(string fileName)
    {
        return _mgr.IsValid(fileName);
    }
}