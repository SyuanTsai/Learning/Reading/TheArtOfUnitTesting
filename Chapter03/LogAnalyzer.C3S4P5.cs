﻿namespace Chapter03;

public class LogAnalyzerC3S4P5
{
    private IFileExtensionManagerCH3S4P1 _mgr;

    public LogAnalyzerC3S4P5()
    {
        _mgr = new FileExtensionManagerCH3S4P1();
    }
    
    public IFileExtensionManagerCH3S4P1 ExtensionManager
    {
        get => _mgr;
        set => _mgr = value ?? throw new ArgumentNullException(nameof(value));
    }

    public bool IsValidLogFileName(string fileName)
    {
        return _mgr.IsValid(fileName);
    }
}