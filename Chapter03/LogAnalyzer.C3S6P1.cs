﻿namespace Chapter03;

public class LogAnalyzerC3S6P1
{
    private IFileExtensionManagerCH3S4P1 _mgr;
    
    internal LogAnalyzerC3S6P1(IFileExtensionManagerCH3S4P1 mgr)
    {
        _mgr = mgr;
    }

    public bool IsValidLogFileName(string fileName)
    {
        return _mgr.IsValid(fileName);
    }
}