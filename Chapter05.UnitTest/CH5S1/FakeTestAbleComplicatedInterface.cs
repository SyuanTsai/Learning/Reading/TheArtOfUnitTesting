﻿namespace Chapter05.Test.CH5S1;

public class FakeTestAbleComplicatedInterface : IComplicatedInterface
{
    public string method1A;
    public string method1B, method2B;
    public bool method1C, method2C, method3C;
    public int method1X, method2X, method3X;
    public int method1O, method2O, method3O;
    
    public void method1(string a, string b, bool c, int x, object o)
    {
        method1A = a;
        method1B = b;
        method1C = c;
        method1X = x;
        method1O = 0;
    }

    public void method2(string b, bool c, int x, object o)
    {
        method2B = b;
        method2C = c;
        method2X = x;
        method2O = 0;
    }

    public void method3(bool c, int x, object o)
    {
        method3C = c;
        method3X = x;
        method3O = 0;
    }
}