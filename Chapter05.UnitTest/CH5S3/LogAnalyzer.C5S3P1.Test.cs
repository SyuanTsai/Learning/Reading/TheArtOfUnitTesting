using NSubstitute;

namespace Chapter05.Test.CH5S3;

public class LogAnalyzerC5S3P1Test
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Analyze_LoggerThrows_CallsWebServiceWithNSubObject()
    {
        var stubLogger     = Substitute.For<ILogger>();
        var mockWebService = Substitute.For<IWebService>();

        stubLogger.When(x => x.LogError(Arg.Any<string>()))
                  .Do(x => { throw new Exception("fake exception"); });

        var log = new LogAnalyzerC5S3P1(stubLogger, mockWebService);
        log.MinNameLength = 10;
        log.Analyze("Short.ext");

        mockWebService.Received()
                      .Write
                      (
                          // 針對期望的物件類型所使用的強行別參數匹配器
                          Arg.Is<ErrorInfo>(info => info.Severity == 1000 && info.Message.Contains("fake exception"))
                      );
    }
    
    
    [Test]
    public void Analyze_LoggerThrows_CallsWebServiceWithNSubObjectCompareTo()
    {
        var stubLogger     = Substitute.For<ILogger>();
        var mockWebService = Substitute.For<IWebService>();

        stubLogger.When(x => x.LogError(Arg.Any<string>()))
                  .Do(x => { throw new Exception("fake exception"); });

        var log = new LogAnalyzerC5S3P1(stubLogger, mockWebService);
        log.MinNameLength = 10;
        log.Analyze("Short.ext");

        var expected = new ErrorInfo(1000, "fake exception");

        mockWebService.Received()
                      .Write(expected);
    }
}