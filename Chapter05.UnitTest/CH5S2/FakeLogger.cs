﻿namespace Chapter05.Test.CH5S2;

public class FakeLogger : ILogger
{
    public string LastError;

    public void LogError(string message)
    {
        LastError = message;
    }
}