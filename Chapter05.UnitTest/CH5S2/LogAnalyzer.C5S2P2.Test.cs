using NSubstitute;

namespace Chapter05.Test.CH5S2;

public class LogAnalyzerC5S2P2Test
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Analyze_TooShortFileName_CallsLogger()
    {
        FakeLogger fakeLogger = new FakeLogger();
        LogAnalyzerC5S2P2 log = new LogAnalyzerC5S2P2(fakeLogger);
        log.MinNameLength = 6;
        log.Analyze("abc.ext");
        StringAssert.Contains("too short", fakeLogger.LastError);
    }
    
    [Test]
    public void Analyze_TooShortFileName_CallsLogger_UsingMock()
    {
        ILogger mockLogger =  Substitute.For<ILogger>();
        LogAnalyzerC5S2P2 log = new LogAnalyzerC5S2P2(mockLogger);
        log.MinNameLength = 6;
        log.Analyze("abc.ext");
        mockLogger.Received().LogError("Filename too short: abc.ext");
    }
    
}