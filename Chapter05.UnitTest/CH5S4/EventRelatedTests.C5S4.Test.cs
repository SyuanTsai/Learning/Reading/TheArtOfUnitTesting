using Chapter05.CH5S4;
using NSubstitute;

namespace Chapter05.Test.CH5S4;

public class EventRelatedTestsC5S4Test
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void ctor_WhenViewIsLoaded_CallsRender()
    {
        // Arrange
        var mockView  = Substitute.For<IView>();
        var presenter = new Presenter(mockView);

        // Act
        mockView.Loaded += Raise.Event<Action>();

        // Assert
        mockView.Received()
                .Render(Arg.Is<string>(s => s.Contains("Hello World")));
    }
    
    [Test]
    public void ctor_WhenViewHasError_CallsLogger()
    {
        // Arrange
        var stubView  = Substitute.For<IView>();
        var mockLogger = Substitute.For<ILogger>();
        var presenter = new Presenter(stubView, mockLogger);
        
        //stubView.ErrorOccured += Raise.Event<Action<string>>("fake error");
        
        // Assert
        mockLogger.Received()
                  .LogError(Arg.Is<string>(s => s.Contains("fake error")));
    }
}