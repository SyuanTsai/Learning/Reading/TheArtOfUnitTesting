using Chapter05.CH5S4;
using NSubstitute;

namespace Chapter05.Test.CH5S4;

public class EventRelatedTestsC5S4P2Test
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void EventFiringManual()
    {
        bool loadFired = false;
        SomeView view = new SomeView();

        view.Loaded += () => loadFired = true;
        
        view.DoSomethingThatEventuallyFiresTheEvent();
        Assert.IsTrue(loadFired);
    }
}

public class SomeView : IView
{
    public event Action? Loaded;

    public void Render(string text)
    {
        throw new NotImplementedException();
    }

    public void DoSomethingThatEventuallyFiresTheEvent()
    {
        throw new NotImplementedException();
    }
}