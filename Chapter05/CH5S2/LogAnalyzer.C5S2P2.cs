﻿namespace Chapter05;

public class LogAnalyzerC5S2P2
{
    private readonly ILogger _logger;
    public int MinNameLength;

    public LogAnalyzerC5S2P2(ILogger logger)
    {
        this._logger = logger;
    }
    
    public void Analyze(string fileName)
    {
        // ... other code ...
        if (MinNameLength < 8)
        {
            _logger.LogError("Filename too short: "+ fileName);
        }
    }
}