﻿namespace Chapter05.CH5S4;

public class Presenter
{
    private readonly IView   _view;
    private readonly ILogger _logger;

    public Presenter(IView view)
    {
        _view = view;
        this._view.Loaded += OnLoaded;
    }

    public Presenter(IView view, ILogger logger)
    {
        _view            = view;
        _logger = logger;
    }

    private void OnLoaded()
    {
        _view.Render("Hello World");
    }
}