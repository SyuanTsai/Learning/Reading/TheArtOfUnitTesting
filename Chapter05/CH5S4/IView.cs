﻿namespace Chapter05.CH5S4;

public interface IView
{
    event Action Loaded;

    void Render(string text);
}