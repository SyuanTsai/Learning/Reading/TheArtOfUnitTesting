﻿namespace Chapter05;

public interface IWebService
{
    void Write(ErrorInfo info);
}