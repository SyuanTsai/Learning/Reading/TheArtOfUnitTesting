﻿namespace Chapter05;

public class ErrorInfo
{
    public ErrorInfo(int severity, string msg)
    {
        Severity = severity;
        Message = msg;
    }

    public ErrorInfo()
    {
    }

    public int Severity { get; set; }

    public string Message { get; set; }
}