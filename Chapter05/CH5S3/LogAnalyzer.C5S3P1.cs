﻿namespace Chapter05;

public class LogAnalyzerC5S3P1
{
    private readonly ILogger     _logger;
    private readonly IWebService _web;
    public           int         MinNameLength;

    public LogAnalyzerC5S3P1(ILogger logger, IWebService web)
    {
        this._logger         = logger;
        _web = web;
    }
    
    public void Analyze(string fileName)
    {
        // ... other code ...
        
        
        try
        {
            if (MinNameLength < 11)
            {
                _logger.LogError("Filename too short: "+ fileName);
            }
        }
        catch (Exception e)
        {
            ErrorInfo info = new ErrorInfo()
            {
                Severity = 1000,
                Message  = e.Message
            };
            
            _web.Write(info);
        }
    }
}