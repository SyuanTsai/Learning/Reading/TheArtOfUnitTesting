﻿namespace Chapter05;

public interface ILogger
{
    public void LogError(string message);
}