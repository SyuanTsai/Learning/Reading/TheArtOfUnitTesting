﻿namespace Chapter05;

public interface IComplicatedInterface
{
    void method1(string a, string b, bool c, int x, object o);
    void method2(string b, bool c, int x, object o);
    void method3(bool c, int x, object o);
}