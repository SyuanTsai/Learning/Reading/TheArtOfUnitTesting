namespace Chapter03.Test.CH3S4P4;

public class LogAnalyzerC3S4P4Test
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void IsValidFileName_ExtManagerThrowsException_ReturnsFalse()
    {
        FakeExtensionManagerC3S4P4 myFakeManager = new FakeExtensionManagerC3S4P4();
        myFakeManager.WillThrow = new Exception("this is fake");
        LogAnalyzerC3S4P3 log = new LogAnalyzerC3S4P3(myFakeManager);
        bool result = log.IsValidLogFileName("anything.anything");
        Assert.IsFalse(result);
    }
}