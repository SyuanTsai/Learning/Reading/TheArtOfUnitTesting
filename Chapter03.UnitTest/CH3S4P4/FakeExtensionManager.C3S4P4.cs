﻿namespace Chapter03.Test.CH3S4P4;

internal class FakeExtensionManagerC3S4P4 : IFileExtensionManagerCH3S4P1
{
    public bool WillBeValid = false;
    public Exception WillThrow = null;

    public bool IsValid(string fileName)
    {
        if (WillThrow != null)
        {
            throw WillThrow;
        }

        return WillBeValid;
    }
}