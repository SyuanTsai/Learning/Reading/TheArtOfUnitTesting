﻿namespace Chapter03.Test.CH3S4P3;

internal class FakeExtensionManagerC3S4P3 : IFileExtensionManagerCH3S4P1
{
    public bool WillBeValid = false;
    public bool IsValid(string fileName)
    {
        return WillBeValid;
    }
}