namespace Chapter03.Test.CH3S4P3;

public class LogAnalyzerC3S4P3Test
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void IsValidFileName_NameSupportedExtension_ReturnsTrue()
    {
        FakeExtensionManagerC3S4P3 myFakeManager = new FakeExtensionManagerC3S4P3();
        myFakeManager.WillBeValid = true;
        LogAnalyzerC3S4P3 log = new LogAnalyzerC3S4P3(myFakeManager);
        bool result = log.IsValidLogFileName("short.ext");
        Assert.IsTrue(result);
    }
}