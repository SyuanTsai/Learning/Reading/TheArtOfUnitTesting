using Chapter03.Test.CH3S4P4;

namespace Chapter03.Test.CH3S4P6;

public class LogAnalyzerC3S4P62Test
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void IsValidFileName_ExtManagerThrowsException_ReturnsFalse()
    {
        // set up the stub to use, make sure it returns true
        FakeExtensionManagerC3S4P4 stub = new FakeExtensionManagerC3S4P4();
        stub.WillBeValid = true;

        TestAbleLogAnalyzerC3S4P62 logan = new TestAbleLogAnalyzerC3S4P62(stub);
        
        // Assert logic assuming extension is supported
        bool result = logan.IsValidLogFileName("file.ext");
        Assert.IsTrue(result);
    }
}

internal class  FakeExtensionManager : IFileExtensionManagerCH3S4P1
{
    public bool WillBeValid = false;
    public Exception WillThrow = null;

    public bool IsValid(string fileName)
    {
        if (WillThrow != null)
        {
            throw WillThrow;
        }
        return WillBeValid;
    }
}