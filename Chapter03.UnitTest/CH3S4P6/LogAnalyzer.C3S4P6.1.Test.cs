using Chapter03.Test.CH3S4P4;

namespace Chapter03.Test.CH3S4P6;

public class LogAnalyzerC3S4P61Test
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void IsValidFileName_ExtManagerThrowsException_ReturnsFalse()
    {
        // set up the stub to use, make sure it returns true
        FakeExtensionManagerC3S4P4 myFakeManager = new FakeExtensionManagerC3S4P4();
        myFakeManager.WillBeValid = true;


        ExtensionManagerFactoryC3S4P6 factory = new ExtensionManagerFactoryC3S4P6();
        factory.SetManager(myFakeManager);
        
        // create analyzer and inject stub
        LogAnalyzerC3S4P61 log = new LogAnalyzerC3S4P61(factory);
        
        // Assert logic assuming extension is supported
        bool result = log.IsValidLogFileName("anything.anything");
        Assert.IsTrue(result);
    }
}