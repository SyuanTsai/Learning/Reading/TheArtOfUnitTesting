﻿namespace Chapter03.Test.CH3S4P6;

public class TestAbleLogAnalyzerC3S4P62 : LogAnalyzerUsingFactoryMethodC3S4P62
{
    private readonly IFileExtensionManagerCH3S4P1 _mgr;

    public  TestAbleLogAnalyzerC3S4P62(IFileExtensionManagerCH3S4P1 mgr)
    {
        _mgr = mgr;
    }

    protected override IFileExtensionManagerCH3S4P1 GetManager()
    {
        return _mgr;
    }
}