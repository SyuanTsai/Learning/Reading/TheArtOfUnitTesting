using Chapter03.Test.CH3S4P3;

namespace Chapter03.Test.CH3S6;

public class LogAnalyzerC3S6P1Test
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void IsValidFileName_SupportedExtension()
    {
        FakeExtensionManagerC3S4P3 myFakeManager = new FakeExtensionManagerC3S4P3();
        myFakeManager.WillBeValid = true;
        LogAnalyzerC3S6P1 log = new LogAnalyzerC3S6P1(myFakeManager);
        bool result = log.IsValidLogFileName("short.ext");
        Assert.IsTrue(result);
    }
}