﻿namespace Chapter03.Test.CH3S4P1;

//  Stub Aka 假物件
public class AlwaysValidFakeExtensionManager : IFileExtensionManagerCH3S4P1
{
    public bool IsValid(string fileName)
    {
        return true;
    }
}