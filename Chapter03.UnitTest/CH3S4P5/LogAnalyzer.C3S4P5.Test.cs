using Chapter03.Test.CH3S4P4;

namespace Chapter03.Test.CH3S4P5;

public class LogAnalyzerC3S4P5Test
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void IsValidFileName_ExtManagerThrowsException_ReturnsFalse()
    {
        // set up the stub to use, make sure it returns true
        FakeExtensionManagerC3S4P4 myFakeManager = new FakeExtensionManagerC3S4P4();
        myFakeManager.WillBeValid = true;
        
        // create analyzer and inject stub
        LogAnalyzerC3S4P5 log = new LogAnalyzerC3S4P5();
        log.ExtensionManager = myFakeManager;
        
        // Assert logic assuming extension is supported
        bool result = log.IsValidLogFileName("anything.anything");
        Assert.IsTrue(result);
    }
}