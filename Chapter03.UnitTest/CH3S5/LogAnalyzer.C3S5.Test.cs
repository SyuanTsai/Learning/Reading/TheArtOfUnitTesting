namespace Chapter03.Test.CH3S5;

public class LogAnalyzerC3S5Test
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void OverrideTestWithoutStub()
    {
        TestAbleLogAnalyzerC3S5 logan = new TestAbleLogAnalyzerC3S5();
        logan.IsSupported = true;
        bool result = logan.IsValidLogFileName("file.ext");
        Assert.IsTrue(result);
    }
}