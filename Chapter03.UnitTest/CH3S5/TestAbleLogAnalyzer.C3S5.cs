﻿namespace Chapter03.Test.CH3S5;

public class TestAbleLogAnalyzerC3S5 : LogAnalyzerUsingFactoryMethodC3S5
{
    public bool IsSupported;

    // 複寫原本的方法
    protected override bool IsValid(string fileName)
    {
        return IsSupported;
    }
}