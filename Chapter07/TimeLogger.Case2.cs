﻿namespace Chapter07;

public static class TimeLoggerCase2
{
    public static string CreateMessage(string info)
    {
        return SystemTime.Now.ToShortDateString() + " " + info;
    }
}

public class SystemTime
{
    private static DateTime _date { get; set; }
    
    public static void Set(DateTime dateTime)
    {
        // 允許修改目前時間
        _date = dateTime;
    }
    
    public static void Reset()
    {
        // 也可以重置目前時間
        _date = DateTime.MinValue;
    }
    
    public static  DateTime Now
    {
        get
        {
            if (_date != DateTime.MinValue)
            {
                return _date;
            }
            return DateTime.Now;
        }
    }
}