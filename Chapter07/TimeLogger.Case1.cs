﻿namespace Chapter07;

public static class TimeLoggerCase1
{
    public static string CreateMessage(string info)
    {
        return DateTime.Now.ToShortDateString() + " " + info;
    }
}