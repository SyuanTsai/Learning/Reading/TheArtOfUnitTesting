﻿namespace Chapter04.Test.CH4S3;

public class FakeEmailServiceC4S3Case2 : IEmailServiceC4S4
{
    public EmailInfo Email;

    public void SendEmail(EmailInfo emailInfo)
    {
        Email = emailInfo;
    }
}