﻿namespace Chapter04.Test.CH4S3;

public class FakeWebServiceC3S4 : IWebService
{
    public Exception ToThrow;

    public void LogError(string message)
    {
        if (ToThrow != null)
        {
            throw ToThrow;
        }
    }
}