namespace Chapter04.Test.CH4S3;

public class LogAnalyzerC4S4Test
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Analyze_WebServiceThrows_SendsEmailC3S4Case1()
    {
        FakeWebServiceC3S4 service = new FakeWebServiceC3S4();
        service.ToThrow = new Exception("fake exception");
        FakeEmailServiceC4S3Case1 mockEmail = new FakeEmailServiceC4S3Case1();
        LogAnalyzerC4S4Case1 log = new LogAnalyzerC4S4Case1(service, mockEmail);
        string tooShortFileName = "abc.ext";
        log.Analyze(tooShortFileName);
        StringAssert.Contains("someone@somewhere.com", mockEmail.To);
        StringAssert.Contains("fake exception", mockEmail.Body);
        StringAssert.Contains("can't log", mockEmail.Subject);
    }
    
    [Test]
    public void Analyze_WebServiceThrows_SendsEmailC3S4Case2()
    {
        FakeWebServiceC3S4 service = new FakeWebServiceC3S4();
        service.ToThrow = new Exception("fake exception");
        FakeEmailServiceC4S3Case2 mockEmail = new FakeEmailServiceC4S3Case2();
        LogAnalyzerC4S4Case2 log = new LogAnalyzerC4S4Case2(service, mockEmail);
        string tooShortFileName = "abc.ext";
        log.Analyze(tooShortFileName);

        EmailInfo expectedEmail = new EmailInfo()
        {
            Body = "fake exception",
            To = "someone@somewhere.com",
            Subject = "can't log"
        };
        
        Assert.AreEqual(expectedEmail, mockEmail.Email);
        Assert.AreSame(expectedEmail, mockEmail.Email);
    }
}
