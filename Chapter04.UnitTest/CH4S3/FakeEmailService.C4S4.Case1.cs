﻿namespace Chapter04.Test.CH4S3;

public class FakeEmailServiceC4S3Case1 : IEmailServiceC4S3
{
    public string Subject { get; set; }

    public string Body { get; set; }

    public string To { get; set; }

    public void SendEmail(string to, string subject, string body)
    {
        To = to;
        Subject = subject;
        Body = body;
    }
}