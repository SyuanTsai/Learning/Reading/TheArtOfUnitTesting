namespace Chapter04.Test.CH4S3;

public class LogAnalyzerC4S3Test
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Analyze_TooShortFileName_CallsWebService()
    {
        FakeWebServiceC4S3 serviceC4S3 = new FakeWebServiceC4S3();
        LogAnalyzerC4S3 log = new LogAnalyzerC4S3(serviceC4S3);
        string tooShortFileName = "abc.ext";
        log.Analyze(tooShortFileName);
        StringAssert.Contains("Filename too short: " + tooShortFileName, serviceC4S3.LastError);
    }
}