﻿namespace Chapter04.Test.CH4S3;

public class FakeWebServiceC4S3 : IWebService
{
    public string LastError;

    public void LogError(string message)
    {
        LastError = message;
    }
}
