namespace Chapter02.Test;

[TestFixture]
public class LogAnalyzerC2S4Test
{
    [SetUp]
    public void Setup()
    {
    }

    //  2.4 撰寫第一個測試程式
    [Test]
    public void IsValidFileName_BadExtension_ReturnFalse()
    {
        // Arrange 準備
        LogAnalyzer analyzer = new LogAnalyzer();
        // Act 操作
        bool result = analyzer.IsValidLogFileName("filewithbadextension.foo");
        // Assert 驗證
        Assert.False(result);
    }
    
    //  2.4.3 增加正向的測試
    [Test]
    public void IsValidFileName_GoodExtensionLowercase_ReturnTrue()
    {
        // Arrange 準備
        LogAnalyzer analyzer = new LogAnalyzer();
        // Act 操作
        bool result = analyzer.IsValidLogFileName("filewithgoodextension.slf");
        // Assert 驗證
        Assert.True(result);
    }
    
    //  2.4.3 增加正向的測試
    [Test]
    public void IsValidFileName_GoodExtensionUppercase_ReturnTrue()
    {
        // Arrange 準備
        LogAnalyzer analyzer = new LogAnalyzer();
        // Act 操作
        bool result = analyzer.IsValidLogFileName("filewithgoodextension.SLF");
        // Assert 驗證
        Assert.True(result);
    }
}