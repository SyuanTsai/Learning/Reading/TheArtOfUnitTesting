﻿namespace Chapter02.Test;

[TestFixture]
public class LogAnalyzer_C2S7_Test
{
    [Test]
    public void IsValidFileName_WhenCalled_ChangesWasLastFileNameValid()
    {
        LogAnalyzerC2S7 analyzer = new LogAnalyzerC2S7();
        analyzer.IsValidLogFileName("badname.foo");
        // 驗證系統狀態
        Assert.False(analyzer.WasLastFileNameValid);
    }
    
    [TestCase("badname.foo", false)]
    [TestCase("goodname.slf", true)]
    public void IsValidFileName_WhenCalled_ChangesWasLastFileNameValid(string file, bool expected)
    {
        LogAnalyzerC2S7 analyzer = new LogAnalyzerC2S7();
        analyzer.IsValidLogFileName(file);
        // 驗證系統狀態
        Assert.AreEqual(expected, analyzer.WasLastFileNameValid);
    }
}