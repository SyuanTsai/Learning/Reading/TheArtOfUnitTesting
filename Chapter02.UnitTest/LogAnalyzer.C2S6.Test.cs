﻿namespace Chapter02.Test;

[TestFixture]
public class LogAnalyzer_C2S6_Test
{
    private LogAnalyzer _analyzer;
    // 2.6.1
    [SetUp]
    public void Setup()
    {
        _analyzer = new LogAnalyzer();
    }
    // 2.6.1
    [Test]
    public void IsValidFileName_ValidFileLowerCased_ReturnFalse()
    {
        bool result = _analyzer.IsValidLogFileName("whatever.slf");
        Assert.True(result, "檔案名稱應該要是有效的");
    }
    // 2.6.1
    [Test]
    public void IsValidFileName_ValidFileUpperCased_ReturnFalse()
    {
        bool result = _analyzer.IsValidLogFileName("whatever.SLF");
        Assert.True(result, "檔案名稱應該要是有效的");
    }
    
    // 2.6.1
    [TearDown]
    public void TearDown()
    {
        // 下面程式是為了呈現一個反模式，並不需要這樣做，請不要在實務上這樣寫。
        _analyzer = null; 
    }
    
    
    // 2.6.2
    [Test]
    public void IsValidFileName_EmptyFileName_ThrowsException()
    {
        Assert.Throws<ArgumentException>(
            () => _analyzer.IsValidLogFileName(string.Empty)
        );
    }
    
    // 2.6.3 忽略此測試
    [Test]
    [Ignore("這個測試有問題。")]
    public void IsValidFileName_ValidFile_ReturnTrue()
    {
        Assert.Fail();
    }
    
    // 2.6.4
    
    // 2.6.3 忽略此測試
    [Test]
    public void IsValidFileName_ValidFileName_ThrowFluent()
    {
        var ex = Assert.Catch<ArgumentException>(
            () => _analyzer.IsValidLogFileName(string.Empty)
        );
        Assert.That(ex.Message, Does.Contain("檔案名稱必須要提供"));
    }
    
    [Test]
    [Category("測試分類")]
    public void IsValidFileName_ValidFile_ReturnTrue_Category()
    {
        Assert.Pass();
    }
}