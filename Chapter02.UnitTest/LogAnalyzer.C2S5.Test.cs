﻿namespace Chapter02.Test;

[TestFixture]
public class LogAnalyzer_C2S5_Test
{
    //  2.5.1.A 重構測試程式
    [TestCase("filewithbadextension.SLF")] //  TestCase特性傳入一個參數給測試方法
    //  2.5.1.B
    [TestCase("filewithbadextension.slf")] //   新增的另一個特性代表新增了另一個傳入不同參數值的測試案例
    public void IsValidFileName_ValidExtension_ReturnFalse(string file) //  測試方法接受一個參數
    {
        // Arrange 準備
        LogAnalyzer analyzer = new LogAnalyzer();
        // Act 操作
        bool result = analyzer.IsValidLogFileName(file);// 這個參數使用就像一般方法一樣。
        // Assert 驗證
        Assert.True(result);
    }

}