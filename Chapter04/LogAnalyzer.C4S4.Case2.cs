﻿namespace Chapter04;

public class LogAnalyzerC4S4Case2
{
    public readonly IWebService _service;
    public readonly IEmailServiceC4S4 _email;

    public LogAnalyzerC4S4Case2(IWebService service, IEmailServiceC4S4 email)
    {
        _service = service;
        _email = email;
    }
    
    
    public void Analyze(string fileName)
    {
        try
        {
            if (fileName.Length < 8)
            {
                _service.LogError("Filename too short: " + fileName);
            }
        }
        catch (Exception e)
        {
            EmailInfo expectedEmail = new EmailInfo()
            {
                Body = e.Message,
                To = "someone@somewhere.com",
                Subject = "can't log"
            };
            
            _email.SendEmail(expectedEmail);
        }
    }
}