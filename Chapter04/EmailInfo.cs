﻿namespace Chapter04;

public class EmailInfo
{
    public string To;
    public string Subject;
    public string Body;
}