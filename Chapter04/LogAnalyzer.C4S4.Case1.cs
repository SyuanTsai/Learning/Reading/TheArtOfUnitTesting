﻿namespace Chapter04;

public class LogAnalyzerC4S4Case1
{
    public readonly IWebService _service;
    public readonly IEmailServiceC4S3 _email;

    public LogAnalyzerC4S4Case1(IWebService service, IEmailServiceC4S3 email)
    {
        _service = service;
        _email = email;
    }
    
    
    public void Analyze(string fileName)
    {
        try
        {
            if (fileName.Length < 8)
            {
                _service.LogError("Filename too short: " + fileName);
            }
        }
        catch (Exception e)
        {
            _email.SendEmail("someone@somewhere.com", "can't log", e.Message);
        }
    }
}