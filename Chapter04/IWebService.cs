﻿namespace Chapter04;

public interface IWebService
{
    void LogError(string message);
}