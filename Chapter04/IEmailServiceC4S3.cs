﻿namespace Chapter04;

public interface IEmailServiceC4S3
{
    void SendEmail(string to, string subject, string body);
}