﻿namespace Chapter04;

public class LogAnalyzerC4S3
{
    private readonly IWebService _service;

    public LogAnalyzerC4S3(IWebService service)
    {
        this._service = service;
    }
    public void Analyze(string fileName)
    {
        if (fileName.Length < 8)
        {
            _service.LogError("Filename too short: " + fileName);
        }
    }
}