﻿namespace Chapter04;

public interface IEmailServiceC4S4
{
    void SendEmail(EmailInfo emailInfo);
}