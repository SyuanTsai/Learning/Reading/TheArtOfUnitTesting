﻿namespace Chapter01;

public class SimpleParserTests
{
    public static void TestReturnsZeroWhenEmptyString()
    {
        try
        {
            var p = new SimpleParser();
            int result = p.ParseAnsSum(string.Empty);

            if (result != 0)
            {
                Console.WriteLine(
                    @"***SimpleParserTests.TestReturnZeroWhenEmptyString:
        ------
        Parse and sum should have returned 0 on an empty string.");
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}