﻿namespace Chapter01;

public class SimpleParser
{
    public int ParseAnsSum(string numbers)
    {
        if (numbers.Length == 0)
        {
            return 0;
        }

        if (!numbers.Contains(","))
        {
            return int.Parse(numbers);
        }
        else
        {
            throw new InvalidOperationException(
                "I Can Only handle 0 or 1 numbers for now!");
        }
    }
}